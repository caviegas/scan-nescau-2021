<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/nescau/modules/content_banner/templates/banner-block.html.twig */
class __TwigTemplate_0b94d2b2837aae2fd30ba63bc88218a357b9d41b00bba7fd7795f7d1fd6974de extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["banners"] = twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "banners", [], "any", false, false, true, 1);
        // line 2
        echo "
";
        // line 3
        if ( !twig_test_empty(($context["banners"] ?? null))) {
            // line 4
            echo "
    <h1>BANNERS</h1>

    ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["banners"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                // line 8
                echo "
        ";
                // line 9
                $context["title"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["banner"], "title", [], "any", false, false, true, 9), "value", [], "any", false, false, true, 9);
                // line 10
                echo "        ";
                $context["body"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["banner"], "content_body", [], "any", false, false, true, 10), "value", [], "any", false, false, true, 10);
                // line 11
                echo "        ";
                $context["image"] = ["uri" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 12
$context["banner"], "content_image", [], "any", false, true, true, 12), "entity", [], "any", false, true, true, 12), "uri", [], "any", false, true, true, 12), "value", [], "any", true, true, true, 12)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["banner"], "content_image", [], "any", false, true, true, 12), "entity", [], "any", false, true, true, 12), "uri", [], "any", false, true, true, 12), "value", [], "any", false, false, true, 12), 12, $this->source), "")) : ("")), "alt" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 13
$context["banner"], "content_image", [], "any", false, true, true, 13), "alt", [], "any", true, true, true, 13)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["banner"], "content_image", [], "any", false, true, true, 13), "alt", [], "any", false, false, true, 13), 13, $this->source), $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 13, $this->source))) : (($context["title"] ?? null))), "style" => "banner_main"];
                // line 16
                echo "        ";
                $context["link"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["banner"], "content_link", [], "any", false, false, true, 16), "value", [], "any", false, false, true, 16);
                // line 17
                echo "        ";
                $context["calltoaction"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["banner"], "content_calltoaction", [], "any", false, false, true, 17), "value", [], "any", false, false, true, 17);
                // line 18
                echo "
        <fieldset>
            <strong>Title:</strong> ";
                // line 20
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 20, $this->source), "html", null, true);
                echo "
            <br>
            <strong>Body:</strong> ";
                // line 22
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(($context["body"] ?? null), 22, $this->source));
                echo "
            <br>
            <strong>link: </strong><a href=\"";
                // line 24
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["link"] ?? null), 24, $this->source), "html", null, true);
                echo "\" target=\"_blank\">clique aqui</a>
            <br>
            <strong>CallToAction:</strong> ";
                // line 26
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["calltoaction"] ?? null), 26, $this->source), "html", null, true);
                echo "
            <br>
            ";
                // line 28
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\twig_tweak\TwigExtension']->drupalImage($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "uri", [], "any", false, false, true, 28), 28, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "style", [], "any", false, false, true, 28), 28, $this->source), ["alt" => twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "alt", [], "any", false, false, true, 28)], true), "html", null, true);
                echo "
        </fieldset>
        
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "modules/custom/nescau/modules/content_banner/templates/banner-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 32,  98 => 28,  93 => 26,  88 => 24,  83 => 22,  78 => 20,  74 => 18,  71 => 17,  68 => 16,  66 => 13,  65 => 12,  63 => 11,  60 => 10,  58 => 9,  55 => 8,  51 => 7,  46 => 4,  44 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/nescau/modules/content_banner/templates/banner-block.html.twig", "/Applications/MAMP/htdocs/dig0026573-beverage-nescau-brazil/docroot/modules/custom/nescau/modules/content_banner/templates/banner-block.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 3, "for" => 7);
        static $filters = array("default" => 12, "escape" => 20, "raw" => 22);
        static $functions = array("drupal_image" => 28);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['default', 'escape', 'raw'],
                ['drupal_image']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
