<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/nescau_new/templates/page.html.twig */
class __TwigTemplate_c2cf4f83cd1d1a28d1505c22ddd8ad6165c46f664fe0898d3e415eb36d058863 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "
";
        // line 43
        $context["classes"] = twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "classes", [], "any", false, false, true, 43);
        // line 44
        echo "
";
        // line 55
        echo "
<h1>Teste</h1>

  <header class=\"content-header clearfix\">
    <div class=\"layout-container\">
      ";
        // line 60
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 60), 60, $this->source), "html", null, true);
        echo "
      ";
        // line 61
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 61), 61, $this->source), "html", null, true);
        echo "
    </div>
  </header>

  <div class=\"layout-container\">
    ";
        // line 66
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "pre_content", [], "any", false, false, true, 66), 66, $this->source), "html", null, true);
        echo "
    <main class=\"page-content clearfix\" role=\"main\">
      <div class=\"visually-hidden\"><a id=\"main-content\" tabindex=\"-1\"></a></div>
      ";
        // line 69
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 69), 69, $this->source), "html", null, true);
        echo "

      ";
        // line 72
        echo "
      ";
        // line 73
        if ( !twig_test_empty(($context["classes"] ?? null))) {
            // line 74
            echo "
        ";
            // line 75
            $context["class_path"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("nescau_product.list_products");
            // line 76
            echo "        ";
            $context["image_style"] = "class";
            // line 77
            echo "
        <fieldset>
          <h3>";
            // line 79
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "em_po", [], "any", false, false, true, 79), "name", [], "any", false, false, true, 79), "value", [], "any", false, false, true, 79), 79, $this->source), "html", null, true);
            echo "</h3>
          <br>
          ";
            // line 81
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "em_po", [], "any", false, false, true, 81), "description", [], "any", false, false, true, 81), "value", [], "any", false, false, true, 81), 81, $this->source));
            echo "
          <br>
          ";
            // line 83
            $context["class_image"] = ["uri" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 84
($context["classes"] ?? null), "em_po", [], "any", false, true, true, 84), "class_image", [], "any", false, true, true, 84), "entity", [], "any", false, true, true, 84), "uri", [], "any", false, true, true, 84), "value", [], "any", true, true, true, 84)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "em_po", [], "any", false, true, true, 84), "class_image", [], "any", false, true, true, 84), "entity", [], "any", false, true, true, 84), "uri", [], "any", false, true, true, 84), "value", [], "any", false, false, true, 84), 84, $this->source), "")) : ("")), "alt" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 85
($context["classes"] ?? null), "em_po", [], "any", false, true, true, 85), "class_image", [], "any", false, true, true, 85), "alt", [], "any", true, true, true, 85)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "em_po", [], "any", false, true, true, 85), "class_image", [], "any", false, true, true, 85), "alt", [], "any", false, false, true, 85), 85, $this->source), "Categoria: Em Pó")) : ("Categoria: Em Pó")), "style" =>             // line 86
($context["image_style"] ?? null)];
            // line 88
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\twig_tweak\TwigExtension']->drupalImage($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "uri", [], "any", false, false, true, 88), 88, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "style", [], "any", false, false, true, 88), 88, $this->source), ["alt" => twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "alt", [], "any", false, false, true, 88)], true), "html", null, true);
            echo "
          <br>
          <a href=\"";
            // line 90
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["class_path"] ?? null), 90, $this->source) . ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "em_po", [], "any", false, true, true, 90), "class_anchor", [], "any", false, true, true, 90), "value", [], "any", true, true, true, 90)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "em_po", [], "any", false, true, true, 90), "class_anchor", [], "any", false, true, true, 90), "value", [], "any", false, false, true, 90), 90, $this->source), "")) : (""))), "html", null, true);
            echo "\">VER PRODUTOS</a>
        </fieldset>

        <fieldset>
          <h3>";
            // line 94
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "para_beber", [], "any", false, false, true, 94), "name", [], "any", false, false, true, 94), "value", [], "any", false, false, true, 94), 94, $this->source), "html", null, true);
            echo "</h3>
          <br>
          ";
            // line 96
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "para_beber", [], "any", false, false, true, 96), "description", [], "any", false, false, true, 96), "value", [], "any", false, false, true, 96), 96, $this->source));
            echo "
          <br>
          ";
            // line 98
            $context["class_image"] = ["uri" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 99
($context["classes"] ?? null), "para_beber", [], "any", false, true, true, 99), "class_image", [], "any", false, true, true, 99), "entity", [], "any", false, true, true, 99), "uri", [], "any", false, true, true, 99), "value", [], "any", true, true, true, 99)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "para_beber", [], "any", false, true, true, 99), "class_image", [], "any", false, true, true, 99), "entity", [], "any", false, true, true, 99), "uri", [], "any", false, true, true, 99), "value", [], "any", false, false, true, 99), 99, $this->source), "")) : ("")), "alt" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 100
($context["classes"] ?? null), "para_beber", [], "any", false, true, true, 100), "class_image", [], "any", false, true, true, 100), "alt", [], "any", true, true, true, 100)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "para_beber", [], "any", false, true, true, 100), "class_image", [], "any", false, true, true, 100), "alt", [], "any", false, false, true, 100), 100, $this->source), "Categoria: Para Beber")) : ("Categoria: Para Beber")), "style" =>             // line 101
($context["image_style"] ?? null)];
            // line 103
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\twig_tweak\TwigExtension']->drupalImage($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "uri", [], "any", false, false, true, 103), 103, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "style", [], "any", false, false, true, 103), 103, $this->source), ["alt" => twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "alt", [], "any", false, false, true, 103)], true), "html", null, true);
            echo "
          <br>
          <a href=\"";
            // line 105
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["class_path"] ?? null), 105, $this->source) . ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "para_beber", [], "any", false, true, true, 105), "class_anchor", [], "any", false, true, true, 105), "value", [], "any", true, true, true, 105)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "para_beber", [], "any", false, true, true, 105), "class_anchor", [], "any", false, true, true, 105), "value", [], "any", false, false, true, 105), 105, $this->source), "")) : (""))), "html", null, true);
            echo "\">VER PRODUTOS</a>
        </fieldset>

        <fieldset>
          <h3>";
            // line 109
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "cereal", [], "any", false, false, true, 109), "name", [], "any", false, false, true, 109), "value", [], "any", false, false, true, 109), 109, $this->source), "html", null, true);
            echo "</h3>
          <br>
          ";
            // line 111
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "cereal", [], "any", false, false, true, 111), "description", [], "any", false, false, true, 111), "value", [], "any", false, false, true, 111), 111, $this->source));
            echo "
          <br>
          <br>
          ";
            // line 114
            $context["class_image"] = ["uri" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 115
($context["classes"] ?? null), "cereal", [], "any", false, true, true, 115), "class_image", [], "any", false, true, true, 115), "entity", [], "any", false, true, true, 115), "uri", [], "any", false, true, true, 115), "value", [], "any", true, true, true, 115)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "cereal", [], "any", false, true, true, 115), "class_image", [], "any", false, true, true, 115), "entity", [], "any", false, true, true, 115), "uri", [], "any", false, true, true, 115), "value", [], "any", false, false, true, 115), 115, $this->source), "")) : ("")), "alt" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 116
($context["classes"] ?? null), "cereal", [], "any", false, true, true, 116), "class_image", [], "any", false, true, true, 116), "alt", [], "any", true, true, true, 116)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "cereal", [], "any", false, true, true, 116), "class_image", [], "any", false, true, true, 116), "alt", [], "any", false, false, true, 116), 116, $this->source), "Categoria: Cereal")) : ("Categoria: Cereal")), "style" =>             // line 117
($context["image_style"] ?? null)];
            // line 119
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\twig_tweak\TwigExtension']->drupalImage($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "uri", [], "any", false, false, true, 119), 119, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "style", [], "any", false, false, true, 119), 119, $this->source), ["alt" => twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "alt", [], "any", false, false, true, 119)], true), "html", null, true);
            echo "
          <br>
          <a href=\"";
            // line 121
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["class_path"] ?? null), 121, $this->source) . ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "cereal", [], "any", false, true, true, 121), "class_anchor", [], "any", false, true, true, 121), "value", [], "any", true, true, true, 121)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "cereal", [], "any", false, true, true, 121), "class_anchor", [], "any", false, true, true, 121), "value", [], "any", false, false, true, 121), 121, $this->source), "")) : (""))), "html", null, true);
            echo "\">VER PRODUTOS</a>
        </fieldset>

        <fieldset>
          <h3>";
            // line 125
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "outros_produtos", [], "any", false, false, true, 125), "name", [], "any", false, false, true, 125), "value", [], "any", false, false, true, 125), 125, $this->source), "html", null, true);
            echo "</h3>
          <br>
          ";
            // line 127
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "outros_produtos", [], "any", false, false, true, 127), "description", [], "any", false, false, true, 127), "value", [], "any", false, false, true, 127), 127, $this->source));
            echo "
          <br>
          <br>
          ";
            // line 130
            $context["class_image"] = ["uri" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 131
($context["classes"] ?? null), "outros_produtos", [], "any", false, true, true, 131), "class_image", [], "any", false, true, true, 131), "entity", [], "any", false, true, true, 131), "uri", [], "any", false, true, true, 131), "value", [], "any", true, true, true, 131)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "outros_produtos", [], "any", false, true, true, 131), "class_image", [], "any", false, true, true, 131), "entity", [], "any", false, true, true, 131), "uri", [], "any", false, true, true, 131), "value", [], "any", false, false, true, 131), 131, $this->source), "")) : ("")), "alt" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 132
($context["classes"] ?? null), "outros_produtos", [], "any", false, true, true, 132), "class_image", [], "any", false, true, true, 132), "alt", [], "any", true, true, true, 132)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "outros_produtos", [], "any", false, true, true, 132), "class_image", [], "any", false, true, true, 132), "alt", [], "any", false, false, true, 132), 132, $this->source), "Categoria: Outros Produtos")) : ("Categoria: Outros Produtos")), "style" =>             // line 133
($context["image_style"] ?? null)];
            // line 135
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\twig_tweak\TwigExtension']->drupalImage($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "uri", [], "any", false, false, true, 135), 135, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "style", [], "any", false, false, true, 135), 135, $this->source), ["alt" => twig_get_attribute($this->env, $this->source, ($context["class_image"] ?? null), "alt", [], "any", false, false, true, 135)], true), "html", null, true);
            echo "
          <br>
          <a href=\"";
            // line 137
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["class_path"] ?? null), 137, $this->source) . ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "outros_produtos", [], "any", false, true, true, 137), "class_anchor", [], "any", false, true, true, 137), "value", [], "any", true, true, true, 137)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["classes"] ?? null), "outros_produtos", [], "any", false, true, true, 137), "class_anchor", [], "any", false, true, true, 137), "value", [], "any", false, false, true, 137), 137, $this->source), "")) : (""))), "html", null, true);
            echo "\">VER PRODUTOS</a>
        </fieldset>

      ";
        }
        // line 141
        echo "
      ";
        // line 142
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 142)) {
            // line 143
            echo "        <div class=\"help\">
          ";
            // line 144
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 144), 144, $this->source), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 147
        echo "      ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 147), 147, $this->source), "html", null, true);
        echo "
    </main>

  </div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/nescau_new/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 147,  216 => 144,  213 => 143,  211 => 142,  208 => 141,  201 => 137,  195 => 135,  193 => 133,  192 => 132,  191 => 131,  190 => 130,  184 => 127,  179 => 125,  172 => 121,  166 => 119,  164 => 117,  163 => 116,  162 => 115,  161 => 114,  155 => 111,  150 => 109,  143 => 105,  137 => 103,  135 => 101,  134 => 100,  133 => 99,  132 => 98,  127 => 96,  122 => 94,  115 => 90,  109 => 88,  107 => 86,  106 => 85,  105 => 84,  104 => 83,  99 => 81,  94 => 79,  90 => 77,  87 => 76,  85 => 75,  82 => 74,  80 => 73,  77 => 72,  72 => 69,  66 => 66,  58 => 61,  54 => 60,  47 => 55,  44 => 44,  42 => 43,  39 => 42,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/nescau_new/templates/page.html.twig", "/Applications/MAMP/htdocs/dig0026573-beverage-nescau-brazil/docroot/themes/custom/nescau_new/templates/page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 43, "if" => 73);
        static $filters = array("escape" => 60, "raw" => 81, "default" => 84);
        static $functions = array("path" => 75, "drupal_image" => 88);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape', 'raw', 'default'],
                ['path', 'drupal_image']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
