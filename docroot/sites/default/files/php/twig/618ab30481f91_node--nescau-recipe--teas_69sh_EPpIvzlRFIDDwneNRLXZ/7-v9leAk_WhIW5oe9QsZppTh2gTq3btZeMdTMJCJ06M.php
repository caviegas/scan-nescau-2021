<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/nescau_new/templates/nescau_recipe/node/node--nescau-recipe--teaser.html.twig */
class __TwigTemplate_82d715f5de392a8b5d55ed64c75f220ef9a39972eeda53e5b126a3388e8fe85d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["recipe"] = (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["elements"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["#node"] ?? null) : null);
        // line 3
        echo "
";
        // line 5
        $context["title"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["recipe"] ?? null), "title", [], "any", false, false, true, 5), "value", [], "any", false, false, true, 5);
        // line 6
        $context["image"] = ["uri" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 7
($context["recipe"] ?? null), "recipe_image", [], "any", false, true, true, 7), "entity", [], "any", false, true, true, 7), "uri", [], "any", false, true, true, 7), "value", [], "any", true, true, true, 7)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["recipe"] ?? null), "recipe_image", [], "any", false, true, true, 7), "entity", [], "any", false, true, true, 7), "uri", [], "any", false, true, true, 7), "value", [], "any", false, false, true, 7), 7, $this->source), "")) : ("")), "alt" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 8
($context["recipe"] ?? null), "recipe_image", [], "any", false, true, true, 8), "alt", [], "any", true, true, true, 8)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["recipe"] ?? null), "recipe_image", [], "any", false, true, true, 8), "alt", [], "any", false, false, true, 8), 8, $this->source), $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 8, $this->source))) : (($context["title"] ?? null))), "style" => "recipe_preview"];
        // line 11
        $context["class_name"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["recipe"] ?? null), "recipe_class", [], "any", false, false, true, 11), "entity", [], "any", false, false, true, 11), "name", [], "any", false, false, true, 11), "value", [], "any", false, false, true, 11);
        // line 12
        $context["duration"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["recipe"] ?? null), "recipe_duration_minutes", [], "any", false, false, true, 12), "value", [], "any", false, false, true, 12);
        // line 13
        $context["servings"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["recipe"] ?? null), "recipe_servings", [], "any", false, false, true, 13), "value", [], "any", false, false, true, 13);
        // line 14
        $context["link"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["recipe"] ?? null), "recipe_link", [], "any", false, false, true, 14), "value", [], "any", false, false, true, 14);
        // line 15
        echo "
<fieldset>
    <h1>";
        // line 17
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 17, $this->source), "html", null, true);
        echo "</h1>
    <br>
    <strong>Categoria:</strong> ";
        // line 19
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["class_name"] ?? null), 19, $this->source), "html", null, true);
        echo "
    <br>
    <strong>Duração:</strong> ";
        // line 21
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["duration"] ?? null), 21, $this->source), "html", null, true);
        echo " min
    <br>
    <strong>Porções:</strong> ";
        // line 23
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["servings"] ?? null), 23, $this->source), "html", null, true);
        echo "
    <br>
    <strong>Link:</strong> <a href=\"";
        // line 25
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["link"] ?? null), 25, $this->source), "html", null, true);
        echo "\" target=\"_blank\">clique aqui</a>
    <br>
    ";
        // line 27
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\twig_tweak\TwigExtension']->drupalImage($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "uri", [], "any", false, false, true, 27), 27, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "style", [], "any", false, false, true, 27), 27, $this->source), ["alt" => twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "alt", [], "any", false, false, true, 27)], true), "html", null, true);
        echo "
</fieldset>";
    }

    public function getTemplateName()
    {
        return "themes/custom/nescau_new/templates/nescau_recipe/node/node--nescau-recipe--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 27,  82 => 25,  77 => 23,  72 => 21,  67 => 19,  62 => 17,  58 => 15,  56 => 14,  54 => 13,  52 => 12,  50 => 11,  48 => 8,  47 => 7,  46 => 6,  44 => 5,  41 => 3,  39 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/nescau_new/templates/nescau_recipe/node/node--nescau-recipe--teaser.html.twig", "/Applications/MAMP/htdocs/dig0026573-beverage-nescau-brazil/docroot/themes/custom/nescau_new/templates/nescau_recipe/node/node--nescau-recipe--teaser.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 2);
        static $filters = array("default" => 7, "escape" => 17);
        static $functions = array("drupal_image" => 27);

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['default', 'escape'],
                ['drupal_image']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
