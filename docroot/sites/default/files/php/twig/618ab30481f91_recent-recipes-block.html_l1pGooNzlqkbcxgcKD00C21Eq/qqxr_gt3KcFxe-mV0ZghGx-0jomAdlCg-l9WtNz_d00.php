<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/nescau/modules/recipe/templates/recent-recipes-block.html.twig */
class __TwigTemplate_8fa95eb9471da59d8d803bd6664e53bcd963672bf5dd1e8b36b196d853f940fd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["recipes"] = twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "recipes", [], "any", false, false, true, 2);
        // line 3
        echo "
";
        // line 4
        if ( !twig_test_empty(($context["recipes"] ?? null))) {
            // line 5
            echo "
    <h1>RECEITAS</h1>

    ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recipes"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recipe"]) {
                // line 9
                echo "
        ";
                // line 11
                echo "        ";
                $context["title"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["recipe"], "title", [], "any", false, false, true, 11), "value", [], "any", false, false, true, 11);
                // line 12
                echo "        ";
                $context["image"] = ["uri" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 13
$context["recipe"], "recipe_image", [], "any", false, true, true, 13), "entity", [], "any", false, true, true, 13), "uri", [], "any", false, true, true, 13), "value", [], "any", true, true, true, 13)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["recipe"], "recipe_image", [], "any", false, true, true, 13), "entity", [], "any", false, true, true, 13), "uri", [], "any", false, true, true, 13), "value", [], "any", false, false, true, 13), 13, $this->source), "")) : ("")), "alt" => ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 14
$context["recipe"], "recipe_image", [], "any", false, true, true, 14), "alt", [], "any", true, true, true, 14)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["recipe"], "recipe_image", [], "any", false, true, true, 14), "alt", [], "any", false, false, true, 14), 14, $this->source), $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 14, $this->source))) : (($context["title"] ?? null))), "style" => "recipe_carousel"];
                // line 17
                echo "        ";
                $context["class_name"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["recipe"], "recipe_class", [], "any", false, false, true, 17), "entity", [], "any", false, false, true, 17), "name", [], "any", false, false, true, 17), "value", [], "any", false, false, true, 17);
                // line 18
                echo "        ";
                $context["duration"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["recipe"], "recipe_duration_minutes", [], "any", false, false, true, 18), "value", [], "any", false, false, true, 18);
                // line 19
                echo "        ";
                $context["servings"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["recipe"], "recipe_servings", [], "any", false, false, true, 19), "value", [], "any", false, false, true, 19);
                // line 20
                echo "        ";
                $context["link"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["recipe"], "recipe_link", [], "any", false, false, true, 20), "value", [], "any", false, false, true, 20);
                // line 21
                echo "
        <fieldset>
            <strong>Título:</strong> ";
                // line 23
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 23, $this->source), "html", null, true);
                echo "
            <br>
            <strong>Categoria:</strong> ";
                // line 25
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["class_name"] ?? null), 25, $this->source), "html", null, true);
                echo "
            <br>
            <strong>Duração:</strong> ";
                // line 27
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["duration"] ?? null), 27, $this->source), "html", null, true);
                echo " min
            <br>
            <strong>Porções:</strong> ";
                // line 29
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["servings"] ?? null), 29, $this->source), "html", null, true);
                echo "
            <br>
            <strong>Link:</strong> <a href=\"";
                // line 31
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["link"] ?? null), 31, $this->source), "html", null, true);
                echo "\" target=\"_blank\">clique aqui</a>
            <br>
            ";
                // line 33
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\twig_tweak\TwigExtension']->drupalImage($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "uri", [], "any", false, false, true, 33), 33, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "style", [], "any", false, false, true, 33), 33, $this->source), ["alt" => twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "alt", [], "any", false, false, true, 33)], true), "html", null, true);
                echo "
        </fieldset>
        
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipe'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "modules/custom/nescau/modules/recipe/templates/recent-recipes-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 37,  107 => 33,  102 => 31,  97 => 29,  92 => 27,  87 => 25,  82 => 23,  78 => 21,  75 => 20,  72 => 19,  69 => 18,  66 => 17,  64 => 14,  63 => 13,  61 => 12,  58 => 11,  55 => 9,  51 => 8,  46 => 5,  44 => 4,  41 => 3,  39 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/nescau/modules/recipe/templates/recent-recipes-block.html.twig", "/Applications/MAMP/htdocs/dig0026573-beverage-nescau-brazil/docroot/modules/custom/nescau/modules/recipe/templates/recent-recipes-block.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 2, "if" => 4, "for" => 8);
        static $filters = array("default" => 13, "escape" => 23);
        static $functions = array("drupal_image" => 33);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['default', 'escape'],
                ['drupal_image']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
