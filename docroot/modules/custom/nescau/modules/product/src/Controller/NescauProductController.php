<?php

namespace Drupal\nescau_product\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\HttpFoundation\Request;

class NescauProductController extends ControllerBase
{
    /**
     * Entity Product.
     * 
     * @var Drupal\nescau_product\Entity\Product
     */
    protected $product;

    /**
     * Entity Term.
     * 
     * @var Drupal\taxonomy\Entity\Term
     */
    protected $term;

    /**
     * Entity Node.
     * 
     * @var Drupal\node\Entity\Node
     */
    protected $recipe;

    public function __construct(EntityStorageInterface $product, EntityStorageInterface $term, EntityStorageInterface $recipe)
    {
        $this->product = $product;
        $this->term = $term;
        $this->recipe = $recipe;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity_type.manager')->getStorage('nescau_product'),
            $container->get('entity_type.manager')->getStorage('taxonomy_term'),
            $container->get('entity_type.manager')->getStorage('node'),
        );
    }

    public function viewProduct(Request $request)
    {
        /** @var Drupal\nescau_product\Entity\Product $product */
        $product = $request->attributes->get('nescau_product');

        // Unsetting unplished
        if (!empty($product->get('paragraph_nutri'))) {
            foreach ($product->paragraph_nutri as $i => $nutri) {
                if ($nutri->entity->status->value == false) {
                    unset($product->paragraph_nutri[$i]);
                }
            }
        }

        // Unsetting unplished
        // Setting add_nescau property
        if (!empty($product->get('paragraph_meal'))) {
            foreach ($product->paragraph_meal as $i => $meal) {
                if ($meal->entity->status->value == false) {
                    unset($product->paragraph_meal[$i]);
                    continue;
                }
                if (!empty($meal->entity->nutrients)) {
                    foreach ($meal->entity->nutrients as $x => $nutrient) {
                        if ($nutrient->entity->status->value == false) {
                            unset($meal->entity->nutrients[$x]);
                            continue;
                        }
                    }
                }
                $product->paragraph_meal[$i]->entity->add_nescau = false;
            }
        }

        // Unsetting unplished
        if (!empty($product->get('content_howtoprepare'))) {
            foreach ($product->content_howtoprepare as $i => $howtoprepare) {
                if ($howtoprepare->entity->status->value == false) {
                    unset($product->content_howtoprepare[$i]);
                    continue;
                }
            }
        }

        // Unsetting unplished
        if (!empty($product->get('content_howismade'))) {
            foreach ($product->content_howismade as $i => $howismade) {
                if ($howismade->entity->status->value == false) {
                    unset($product->content_howismade[$i]);
                    continue;
                }
            }
        }

        /** @var Drupal\taxonomy\Entity\Term $class */
        $class = $product->getClass();

        // Get all class related products except current product
        $product_ids = $this->product->getQuery()
            ->condition('tid', $class->id())
            ->condition('id', $product->id(), '<>')
            ->execute();
        /** @var Drupal\nescau_product\Entity\Product $products_class */
        $products_class = $this->product->loadMultiple($product_ids);

        // Get all class related recipes (nodes)
        /** @var Drupal\node\Entity\Node $recipes */
        $recipes = $this->recipe->loadByProperties([
            'type' => 'nescau_recipe',
            'recipe_class' => $class->id(),
            'status' => true
        ]);

        $data = [
            'product' => $product,
            'products_class' => $products_class,
            'recipes' => $recipes,
        ];

        return [
            '#theme' => 'product',
            '#data' => $data,
        ];
    }

    public function listProducts()
    {
        /** @var Drupal\taxonomy\Entity\Term $classes */
        $classes = $this->term->loadByProperties([
            'vid' => 'class',
        ]);

        $list = [];
        foreach ($classes as $class) {
            $product_ids = $this->product->getQuery()
                ->condition('tid', $class->id())
                ->sort('changed', 'DESC')
                ->execute();
            /** @var Drupal\nescau_product\Entity\Product $products */
            $products = $this->product->loadMultiple($product_ids);

            $list[] = [
                'class' => $class,
                'class_path' => substr($class->toUrl()->toString(), 1),
                'products' => $products,
            ];
        }

        $data = [
            'list' => $list,
        ];

        return [
            '#theme' => 'products',
            '#data' => $data,
        ];
    }
}