<?php

namespace Drupal\nescau_product;

use Drupal\Core\Controller\ControllerBase;

class NescauController extends ControllerBase
{
    public function addClass()
    {
        $taxonomyVocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');

        $vocabulary = $taxonomyVocabulary->loadByProperties(['vid' => 'class']);

        if (empty($vocabulary)) {
            $vocabulary = $taxonomyVocabulary->create([
                'vid' => 'class',
                'name' => 'Categorias',
                'description' => 'Categorias de produtos',
            ]);
            $vocabulary->save();
        }

        $taxonomyTerm = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

        $terms = $taxonomyTerm->loadByProperties([
            'vid' => 'class',
            'name' => [
                'Em Pó',
                'Para Beber',
                'Cereal',
                'Outros Produtos NESCAU®',
            ],
        ]);

        $classes = [
            'Em Pó' => [
                'name' => 'Em Pó',
                'langcode' => 'pt-br',
                'description' => '<p>Produto em pó.</p>',
                'weight' => 1,
                'anchor' => '#em-po',
                'path' => '/em-po',
            ],
            'Para Beber' => [
                'name' => 'Para Beber',
                'langcode' => 'pt-br',
                'description' => '<p>Produto para beber.</p>',
                'weight' => 2,
                'anchor' => '#para-beber',
                'path' => '/para-beber',
            ],
            'Cereal' => [
                'name' => 'Cereal',
                'langcode' => 'pt-br',
                'description' => '<p>Produto cereal.</p>',
                'weight' => 3,
                'anchor' => '#cereal',
                'path' => '/cereal',
            ],
            'Outros Produtos NESCAU®' => [
                'name' => 'Outros Produtos NESCAU®',
                'langcode' => 'pt-br',
                'description' => '<p>Produto diversos.</p>',
                'weight' => 9,
                'anchor' => '#outros-produtos',
                'path' => '/outros-produtos',
            ],
        ];

        if (!empty($terms)) {
            foreach ($terms as $term) {
                $term_name = $term->get('name')->value;
                if (array_key_exists($term_name, $classes)) {
                    unset($classes[$term_name]);
                }
            }
        }

        foreach ($classes as $class) {
            $term_class = $taxonomyTerm->create([
                'vid' => 'class',
                'langcode' => $class['langcode'],
            ]);
            $term_class->setName($class['name']);
            $term_class->setDescription($class['description']);
            $term_class->setFormat('basic_html');
            $term_class->setWeight($class['weight']);
            $term_class->set('class_anchor', $class['anchor']);
            $term_class->set('path', $class['path']);
            $term_class->save();
        }

        return $view = [
            '#markup' => 'Categorias criadas.',
        ];
    }

    public function addProducts()
    {
        $titles = [
            'Em Pó' => [
                'NESCAU®',
                'NESCAU® 33% Menos Açúcares',
                'NESCAU® MAX Cereal 0% Açúcar',
                'NESCAU® Light'
            ],
            'Para Beber' => [
                'NESCAU® Prontinho',
                'NESCAU® Prontinho 0% Lactose',
                'NESCAU® Prontinho Light',
                'NESCAU® Shake',
                'NESCAU® Shake MAX Cereal 0% Açúcar',
                'NESCAU® Shake MAX Protein',
                'NESCAU® Orgânico',
            ],
            'Cereal' => [
                'NESCAU® Cereal',
                'NESCAU® Duo',
                'NESCAU® Cereal 60% menos açúcares',
            ],
            'Outros Produtos NESCAU®' => [
                'NESCAU® Nescafé® Dolce Gusto',
                'NESCAU® Biscoito',
            ],
        ];

        $productStorage = \Drupal::entityTypeManager()->getStorage('nescau_product');

        $term_em_po = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'class', 'name' => 'Em Pó']);

        $term_em_po = reset($term_em_po);

        foreach ($titles['Em Pó'] as $title) {
            $term = $productStorage->create([
                'title' => $title,
                'tid' => $term_em_po->id(),
            ]);
            $term->save();
        }

        $term_para_beber = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'class', 'name' => 'Para Beber']);

        $term_para_beber = reset($term_para_beber);

        foreach ($titles['Para Beber'] as $title) {
            $term = $productStorage->create([
                'title' => $title,
                'tid' => $term_para_beber->id(),
            ]);
            $term->save();
        }

        $term_cereal = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'class', 'name' => 'Cereal']);

        $term_cereal = reset($term_cereal);

        foreach ($titles['Cereal'] as $title) {
            $term = $productStorage->create([
                'title' => $title,
                'tid' => $term_cereal->id(),
            ]);
            $term->save();
        }

        $term_outros = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'class', 'name' => 'Outros Produtos NESCAU®']);

        $term_outros = reset($term_outros);

        foreach ($titles['Outros Produtos NESCAU®'] as $title) {
            $term = $productStorage->create([
                'title' => $title,
                'tid' => $term_outros->id(),
            ]);
            $term->save();
        }

        return $view = [
            '#markup' => 'Produtos criados.',
        ];
    }
}