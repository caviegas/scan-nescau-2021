<?php

namespace Drupal\nescau_product\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ProductListFilterForm extends FormBase
{
    /**
     * @var use Drupal\taxonomy\Entity\Term
     */
    protected $term;

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "product_filter_form";
    }

    /**
     * Constructs the object.
     */
    public function __construct(EntityTypeManagerInterface $entity)
    {
        $this->term = $entity->getStorage('taxonomy_term');
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('entity_type.manager')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $request = \Drupal::request();

        $form['filter'] = [
            '#type' => 'container',
            '#attributes' => [
                'class' => ['form--inline', 'clearfix'],
            ],
        ];

        /** Field - Title */
        $form['filter']['title'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nome'),
            '#description' => 'Ignora maiúsculas',
            '#default_value' => empty($request->get('title')) ? NULL : $request->get('title'),
        ];

        /** Field - Classes (Taxonomy Term) */
        $tid = $request->get('tid');
        if (!empty($tid)) {
            $ids = [];
            foreach ((array) $tid as $class) {
                if (isset($class['target_id'])) {
                    $ids[] = $class['target_id'];
                }
            }
            /** @var Drupal\taxonomy\Entity\Term $classes */
            $classes = $this->term->loadMultiple($ids);
        }
        $form['filter']['tid'] = [
            '#type' => 'entity_autocomplete',
            '#target_type' => 'taxonomy_term',
            '#handler' => 'default:taxonomy_term',
            '#handler_settings' => [
                'target_bundles' => [
                    'class' => 'class',
                ],
            ],
            '#title' => $this->t('Categoria'),
            '#description' => $this->t('Separe as categorias com vírgula para filtrar mais de uma'),
            '#default_value' => empty($classes) ? NULL : $classes,
            '#tags' => TRUE,
            '#weight' => '0',
        ];

        /** Field - Date Created */
        $form['filter']['created'] = [
            '#type' => 'date',
            '#title' => $this->t('Criado Em'),
            '#date_date_format' => 'd/m/Y',
            '#description' => 'Formato mês/dia/ano',
            '#default_value' => empty($request->get('created')) ? '' : $request->get('created'),
        ];

        /** Form Actions */
        $form['actions']['wrapper'] = [
            '#type' => 'container',
            '#attributes' => ['class' => ['form-item']],
        ];

        $form['actions']['wrapper']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Filtrar'),
        ];

        if ($request->getQueryString()) {
            $form['actions']['wrapper']['reset'] = [
                '#type' => 'submit',
                '#value' => $this->t('Limpar'),
                '#submit' => ['::resetForm'],
            ];
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $query = [];

        if (!empty($form_state->getValue('title'))) {
            $query['title'] = $form_state->getValue('title');
        }

        if (!empty($form_state->getValue('tid'))) {
            $query['tid'] = $form_state->getValue('tid');
        }

        if (!empty($form_state->getValue('created'))) {
            $query['created'] = $form_state->getValue('created');
        }

        $form_state->setRedirect('entity.nescau_product.collection', $query);
    }

    /**
     * {@inheritdoc}
     */
    public function resetForm(array $form, FormStateInterface $form_state)
    {
        $form_state->setRedirect('entity.nescau_product.collection');
    }
}