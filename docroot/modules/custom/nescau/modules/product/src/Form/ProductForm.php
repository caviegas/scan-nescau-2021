<?php

namespace Drupal\nescau_product\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the nescau_product entity edit forms.
 *
 * @ingroup nescau_product
 */
class ProductForm extends ContentEntityForm
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        /** @var $entity \Drupal\nescau_product\Entity\Product */
        $form = parent::buildForm($form, $form_state);
        $entity = $this->entity;
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        $status = parent::save($form, $form_state);
        
        $entity = $this->entity;
        
        if ($status == SAVED_UPDATED) {
            $this->messenger()
                ->addMessage($this->t('O produto %title foi atualizado.', [
                    '%title' => $entity->getTitle(),
            ]));
        } else {
            $this->messenger()
                ->addMessage($this->t('O produto %title foi adicionado.', [
                    '%title' => $entity->getTitle(),
            ]));
        }

        $form_state->setRedirectUrl($this->entity->toUrl('collection'));
        return $status;
    }
}