<?php

namespace Drupal\nescau_product\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a nescau_product entity.
 *
 * @ingroup nescau_product
 */
class ProductDeleteForm extends ContentEntityConfirmFormBase
{
    /**
     * Returns the question to ask the user.
     * 
     * @return string
     *  The form question. The page title will be set to this value.
     */
    public function getQuestion()
    {
        return $this->t('Tem certeza que quer deletar %title?', [
            '%title' => $this->entity->getTitle(),
        ]);
    }

    /**
     * Returns the route to go to if the user cancels the action.
     * 
     * @return \Drupal\Core\Url
     *  A URL object.
     */
    public function getCancelUrl()
    {
        return new Url('entity.nescau_product.collection');
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmText()
    {
        return $this->t('Deletar');
    }

    /**
     * {@inheritdoc}
     * 
     * Delete the entity and log the event. logger() replaces the watchdog.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $entity = $this->getEntity();
        $entity->delete();

        $this->logger('nescau_product')->notice('%title foi deletado.', [
            '%title' => $this->entity->getTitle(),
        ]);

        // Redirect to term list after delete.
        //$form_state->setRedirect('entity.nescau_product.collection');
        $form_state->setRedirectUrl($this->entity->toUrl('collection')); //TODO: Verificar funcionamento
    }

}