<?php

namespace Drupal\nescau_product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProductSettingsForm.
 * @package Drupal\nescau_product\Form
 * @ingroup nescau_product
 */
class ProductSettingsForm extends FormBase
{
    /**
     * Returns a unique string identifying the form.
     * 
     * @return string
     */
    public function getFormId()
    {
        return 'nescau_product_settings';
    }

    /**
     * Form submission handler.
     * 
     * @param array $form
     *  An associative array containing the structure of the form.
     * @param FormStateInterface $form_state
     *  An associative array containing the current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Empty implementation of the abstract submit class.
    }

    /**
     * Define the form used for Product settings.
     * 
     * @return array
     *  Form definition array.
     * @param array $form
     *  An associative array containing the structure of the form.
     * @param FormStateInterface $form_state
     *  An associative array containing the current state of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['product_settings']['#markup'] = 'Configuração para Produto. Configure os campos aqui.';
        return $form;
    }
}