<?php

namespace Drupal\nescau_product;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;

/**
 * Provides a list controller for nescau_product entity.
 *
 * @ingroup nescau_product
 */
class ProductListBuilder extends EntityListBuilder
{
    /**
     * The date formatter.
     *
     * @var \Drupal\Core\Datetime\DateFormatterInterface
     */
    protected $dateFormatter;
    
    /**
     * The url generator.
     *
     * @var \Drupal\Core\Routing\UrlGeneratorInterface
     */
    protected $urlGenerator;

    /**
     * {@inheritdoc}
     */
    public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type)
    {
        return new static(
            $entity_type,
            $container->get('entity_type.manager')->getStorage($entity_type->id()),
            $container->get('url_generator'),
            $container->get('date.formatter')
        );
    }

    /**
     * Constructs a new VehicleListBuilder object.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
     * @param \Drupal\Core\Entity\EntityStorageInterface $storage
     * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
     * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
     */
    public function __construct(
        EntityTypeInterface $entity_type,
        EntityStorageInterface $storage,
        UrlGeneratorInterface $url_generator,
        DateFormatterInterface $date_formatter
    ) {
        parent::__construct($entity_type, $storage);
        $this->urlGenerator = $url_generator;
        $this->dateFormatter = $date_formatter;
    }
    
    /**
     * {@inheritdoc}
     * 
     * We override ::render() so that we can add our own content above the table.
     */
    public function render()
    {
        $build['description'] = [
            '#markup' => $this->t('Produto implementa um modelo de Produtos. Esses produtos podem possuir campos e serem configurados na <a href="@adminlink">página de admin de Produtos</a>.',[
                '@adminlink' => $this->urlGenerator->generateFromRoute('nescau_product.product_settings'),
            ]),
          ];
        // This was done so the form keeps on top and the arg get sent to the form_state object
        $build['form'] = \Drupal::formBuilder()->getForm('Drupal\nescau_product\Form\ProductListFilterForm');
        $build += parent::render();
        return $build;
    }

    /**
     * {@inheritdoc}
     * 
     * Building the header and content lines for the product list.
     * 
     * Calling the parent::buildHeader() adds a column for the possible actions
     * and inserts the 'edit' and 'delete' links as defined for the entity type.
     */
    public function buildHeader()
    {
        $header['id'] = $this->t('ID');
        $header['title'] = $this->t('Nome');
        $header['class'] = $this->t('Categoria');
        $header['created'] = $this->t('Criado em');
        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity)
    {
        /** @var $entity \Drupal\nescau_product\Entity\Product */
        $row['id'] = $entity->id();
        $row['title'] = $entity->getTitle();
        $row['class'] = $entity->getClassName();
        $row['created'] = empty($entity->getCreatedTime()) ? NULL : $this->dateFormatter->format($entity->getCreatedTime(), 'custom', 'd/m/Y');
        return $row + parent::buildRow($entity);
    }
    
    /**
     * 
     */
    protected function getEntityIds()
    {
        $query = \Drupal::entityQuery($this->entityTypeId);
        $request = \Drupal::request();

        if (!empty($request->get('title'))) {
            $query->condition('title', "%{$request->get('title')}%", 'LIKE');
        }
        
        if (!empty($request->get('tid'))) {
            $classes = [];
            foreach ($request->get('tid') as $class) {
                $classes[] = $class['target_id'];
            }
            $query->condition('tid', $classes, 'IN');
        }

        /** Search within 24 hour range. */
        if (!empty($request->get('created'))) {
            $created_start = new DrupalDateTime($request->get('created'));
            $created_end = new DrupalDateTime($request->get('created') . ' 23:59:59');
    
            $query->condition('created', [
                $created_start->getTimestamp(),
                $created_end->getTimestamp(),
            ], 'BETWEEN');
        }

        return $query->execute();
    }
}