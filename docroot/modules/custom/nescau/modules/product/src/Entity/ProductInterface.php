<?php

namespace Drupal\nescau_product\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
//use Drupal\Core\Entity\EntityPublishedInterface;
//use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for products.
 */
interface ProductInterface extends ContentEntityInterface, EntityChangedInterface
{

  /**
   * Gets the about information.
   *
   * @return string
   */
  public function getAbout();

  /**
   * Sets the about information.
   *
   * @param string $about
   *
   * @return $this
   */
  public function setAbout($about);

  /**
   * Gets the class taxonomy term entity.
   * 
   * @return Drupal\taxonomy\Entity\Term
   */
  public function getClass();

  /**
   * Gets the class taxonomy term id.
   * 
   * @return int
   */
  public function getClassId();

  /**
   * Gets the class taxonomy term name.
   * 
   * @return string
   */
  public function getClassName();

  /**
   * Sets the class taxonomy term entity.
   * 
   * @param Drupal\taxonomy\Entity\Term $class
   * 
   * @return $this
   */
  public function setClass(TermInterface $class);

  /**
   * Sets the class taxonomy term id.
   * 
   * @param int
   * 
   * @return $this
   */
  public function setClassId($tid);

  /**
   * Gets the components information.
   *
   * @return string
   */
  public function getComponents();

  /**
   * Sets the components information.
   *
   * @param string $components
   *
   * @return $this
   */
  public function setComponents($components);

  /**
   * Gets the product creation timestamp.
   *
   * @return int
   */
  public function getCreatedTime();

  /**
   * Sets the product creation timestamp.
   *
   * @param int $timestamp
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the product image.
   * 
   * @return \Drupal\Core\Image
   */
  public function getImage();

  /**
   * Sets the product image entity.
   * 
   * @param Drupal\Core\Image
   * 
   * @return $this
   */
  public function setImage(ImageInterface $image);

  /**
   * Gets the product lett ID.
   *
   * @return string
   */
  public function getLettId();

  /**
   * Sets the product lett ID.
   *
   * @param string $lett_id
   *
   * @return $this
   */
  public function setLettId($lett_id);

  /**
   * Gets the product nutritional information.
   *
   * @return string
   */
  public function getNutritional();

  /**
   * Sets the product nutritional information.
   *
   * @param string $nutritional
   *
   * @return $this
   */
  public function setNutritional($nutritional);

  /**
   * Gets the product preservation information.
   *
   * @return string
   */
  public function getPreservation();

  /**
   * Sets the product preservation information.
   *
   * @param string $preservation
   *
   * @return $this
   */
  public function setPreservation($preservation);

  /**
   * Gets the product title.
   *
   * @return string
   */
  public function getTitle();

  /**
   * Sets the product title.
   *
   * @param string $title
   *
   * @return $this
   */
  public function setTitle($title);

}