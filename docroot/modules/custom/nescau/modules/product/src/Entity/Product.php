<?php

namespace Drupal\nescau_product\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\nescau_product\Entity\ProductInterface;

/**
 * Defines the product entity class.
 * 
 * @ingroup product
 * 
 * @ContentEntityType(
 *  id = "nescau_product",
 *  label = @Translation("Entidade Produto"),
 *  label_collection = @Translation("Produtos"),
 *  label_singular = @Translation("Produto"),
 *  label_plural = @Translation("Produtos"),
 *  label_count = @PluralTranslation(
 *      singular = "@count produto",
 *      plural = "@count produtos",
 *  ),
 *  handlers = {
 *      "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *      "list_builder" = "Drupal\nescau_product\ProductListBuilder",
 *      "views_data" = "Drupal\nescau_product\ProductViewsData",
 *      "form" = {
 *          "add" = "Drupal\nescau_product\Form\ProductForm",
 *          "edit" = "Drupal\nescau_product\Form\ProductForm",
 *          "delete" = "Drupal\nescau_product\Form\ProductDeleteForm",
 *      },
 *      "access" = "Drupal\nescau_product\ProductAccessControlHandler",
 *  },
 *  admin_permission = "administer nescau_product entity",
 *  fieldable = TRUE,
 *  base_table = "nescau_product",
 *  entity_keys = {
 *      "id" = "id",
 *      "label" = "title",
 *      "uuid" = "uuid",
 *      "tid" = "tid",
 *      "created" = "created",
 *      "changed" = "changed",
 *  },
 *  links = {
 *      "canonical" = "/produto/{nescau_product}",
 *      "add-form" = "/produto/{nescau_product}/add/",
 *      "edit-form" = "/produto/{nescau_product}/editar",
 *      "delete-form" = "/produto/{nescau_product}/deletar",
 *      "delete-multiple-form" = "/admin/nescau_product/products/delete",
 *      "collection" = "/nescau_product/produtos",
 *  },
 *  field_ui_base_route = "nescau_product.product_settings",
 *  common_reference_target = TRUE,
 * )
 */

class Product extends ContentEntityBase implements ProductInterface
{
    // Implements methods defined by EntityChangedInterface.
    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public function getAbout()
    {
        return $this->get('about')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setAbout($about)
    {
        $this->set('about', $about);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->get('tid')->entity;
    }

    /**
     * {`@inheritdoc}
     */
    public function getClassId()
    {
        return $this->get('tid')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getClassName()
    {
        return $this->getClass()->get('name')->value;
    }

    /**
     * {`@inheritdoc}
     */
    public function setClass(TermInterface $class)
    {
        $this->set('tid', $class->id());
        return $this;
    }

    /**
     * {`@inheritdoc}
     */
    public function setClassId($tid)
    {
        $this->set('tid', $tid);
        return $this;
    }

    /**
     * Gets the path from term "class" linked to the product.
     * 
     * @return string
     */
    public function getClassPathAlias()
    {
        return substr($this->getClass()->toUrl()->toString(), 1);
    }

    /**
     * {@inheritdoc}
     */
    public function getComponents()
    {
        return $this->get('about')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setComponents($components)
    {
        $this->set('components', $components);
        return $this;
    }

    /**
     * Get the content "how is made" from the product.
     * 
     * @return array Drupal\paragraphs\Entity\Paragraph
     *  Returns an array of Paragraph objects if it exists
     */
    public function getContentHowismade()
    {
        return $this->get('content_howismade')->entity;
    }

    /**
     * Set the content "how is made" to the product.
     * 
     * @param Drupal\paragraphs\Entity\Paragraph $content_howismade
     * 
     * @return $this
     */
    public function setContentHowismade($content_howismade)
    {
        $this->set('content_howismade', $content_howismade);
        return $this;
    }

    /**
     * Get the content "how to prepare" from the product.
     * 
     * @return array Drupal\paragraphs\Entity\Paragraph
     *  Returns an array of Paragraph objects if it exists
     */
    public function getContentHowtoprepare()
    {
        return $this->get('content_howtoprepare')->entity;
    }

    /**
     * Set the content "how to prepare" to the product.
     * 
     * @param Drupal\paragraphs\Entity\Paragraph $content_howtoprepare
     * 
     * @return $this
     */
    public function setContentHowtoprepare($content_howtoprepare)
    {
        $this->set('content_howtoprepare', $content_howtoprepare);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getImage()
    {
        return $this->get('image')->entity;
    }

    /**
     * {`@inheritdoc}
     */
    public function setImage(ImageInterface $image)
    {
        $this->set('image', $image->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLettId()
    {
        return $this->get('lett_id')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setLettId($lett_id)
    {
        $this->set('lett_id', $lett_id);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getNutritional()
    {
        return $this->get('nutritional')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setNutritional($nutritional)
    {
        $this->set('nutritional', $nutritional);
        return $this;
    }

    /**
     * Gets the path from product.
     * 
     * @return string
     */
    public function getPathAlias()
    {
        return substr($this->toUrl()->toString(), 1);
    }

    /**
     * {@inheritdoc}
     */
    public function getPreservation()
    {
        return $this->get('preservation')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setPreservation($preservation)
    {
        $this->set('preservation', $preservation);
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->get('title')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->set('title', $title);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);
        //$fields += static::ownerBaseFieldDefinitions($entity_type); //! checar se necessário
        //$fields += static::publishedBaseFieldDefinitions($entity_type); //! checar se necessário

        // Standard field, used as unique if primary index.
        $fields['id'] = BaseFieldDefinition::create('integer')
            ->setLabel(t('ID'))
            ->setDescription(t('O ID da entidade Produto.'))
            ->setReadOnly(TRUE);

        // Standard field, unique outside of the scope of the current project.
        $fields['uuid'] = BaseFieldDefinition::create('uuid')
            ->setLabel(t('UUID'))
            ->setDescription(t('O UUID da entidade Produto.'))
            ->setReadOnly(TRUE);

        // Title field for the product.
        $fields['title'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Título'))
            ->setDescription(t('O título do produto.'))
            ->setRequired(TRUE)
            ->setTranslatable(TRUE)
            ->setSettings([
                'default_value' => '',
                'max_length' => 255,
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => 0,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['path'] = BaseFieldDefinition::create('path')
            ->setLabel(t('URL amigável'))
            ->setDescription(t('A URL amigável do produto.'))
            ->setTranslatable(TRUE)
            ->setDisplayOptions('form', [
                'type' => 'path',
                'weight' => 1,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setComputed(TRUE);

        /**
         * Taxonomy field for the product.
         * Entity reference field, holds the reference to the taxonomy object.
         * The view shows the taxonomy name.
         * The form presents an auto complete field for the taxonomy name.
         */
        $fields['tid'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Categoria'))
            ->setDescription(t('A categoria do produto.'))
            ->setRequired(TRUE)
            ->setSettings([
                'target_type' => 'taxonomy_term',
                'handler' => 'default:taxonomy_term',
                'handler_settings' => [
                    'target_bundles' => [
                        'class' => 'class',
                    ],
                ],
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'entity_reference_label',
                'weight' => 2,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 2,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => 60,
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        // Image field for the product.
        $fields['image'] = BaseFieldDefinition::create('image')
            ->setLabel(t('Imagem'))
            ->setDescription(t('A imagem do produto.'))
            ->setRequired(FALSE)
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'type' => 'image',
                'weight' => 3,
                'label' => 'hidden',
                'settings' => [
                    'image_style' => 'large',
                ],
            ])
            ->setDisplayOptions('form',  [
                'type' => 'image',
                'weight' => 3,
                'label' => 'hidden',
                'settings' => [
                    'image_style' => 'thumbnail',
                ]
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE)
            ->setReadOnly(TRUE);

        // About field for the product.
        $fields['about'] = BaseFieldDefinition::create('text_long')
            ->setLabel(t('Sobre o produto'))
            ->setDescription(t('A informação sobre o produto.'))
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'text_textarea_with_summary',
                'weight' => 4,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 4,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        // Lett ID field for the product.
        $fields['lett_id'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Lett ID'))
            ->setDescription(t('O lett ID do produto.'))
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'max_length' => 255,
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => 5,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => 5,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        // Components field for the product.
        $fields['components'] = BaseFieldDefinition::create('text_long')
            ->setLabel(t('Ingredientes'))
            ->setDescription(t('Os ingredientes do produto.'))
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'text_textarea_with_summary',
                'weight' => 6,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 6,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        // Preservation field for the product.
        $fields['preservation'] = BaseFieldDefinition::create('text_long')
            ->setLabel(t('Conservação'))
            ->setDescription(t('As informações de conservação do produto.'))
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'text_textarea_with_summary',
                'weight' => 7,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 7,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        // Nutritional field for the product.
        // This field is probably a HTML with table tags.
        $fields['nutritional'] = BaseFieldDefinition::create('text_long')
            ->setLabel(t('Informações Nutricionais'))
            ->setDescription(t('As informações nutricionais do produto.'))
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'text_textarea_with_summary',
                'weight' => 8,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 8,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['content_howismade'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Conteúdo - Como é feito'))
            ->setDescription(t('O bloco de conteúdo "como é feito".'))
            ->setRequired(FALSE)
            ->setSettings([
                'target_type' => 'node',
                'handler' => 'default',
                'handler_settings' => [
                    'target_bundles' => [
                        'nescau_content_howismade' => 'nescau_content_howismade'
                    ],
                ],
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'entity_reference_label',
                'weight' => 9,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => 60,
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
                'weight' => 9,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);
        
        $fields['content_howtoprepare'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Conteúdo - Como preparar'))
            ->setDescription(t('O bloco de conteúdo "como preparar".'))
            ->setRequired(FALSE)
            ->setSettings([
                'target_type' => 'node',
                'handler' => 'default',
                'handler_settings' => [
                    'target_bundles' => [
                        'nescau_content_howtoprepare' => 'nescau_content_howtoprepare'
                    ],
                ],
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'entity_reference_label',
                'weight' => 10,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => 60,
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
                'weight' => 10,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        // Paragraph graphic nutritional information
        $fields['paragraph_nutri'] = BaseFieldDefinition::create('entity_reference_revisions')
                ->setLabel(t('Gráfico de Informações Nutricionais'))
                ->setDescription(t('O gráfico de informações nutricionais do produto.'))
                ->setRequired(FALSE)
                ->setCardinality(-1)
                ->setRevisionable(TRUE)
                ->setSetting('target_type', 'paragraph')
                ->setSetting('handler', 'default:paragraph')
                ->setSetting('handler_settings', [
                    'target_bundles' => [
                        'product_info_nutri' => 'product_info_nutri'
                    ],
                ])
                ->setTranslatable(FALSE)
                ->setDisplayOptions('form', [
                    'type' => 'entity_reference_paragraphs',
                    'weight' => 11,
                    'settings' => [
                        'title' => 'Conteúdo',
                        'title_plural' => 'Conteúdos',
                        'edit_mode' => 'closed_expand_nested',
                        'closed_mode' => 'summary',
                        'autocollapse' => 'none',
                        'add_mode' => 'modal',
                        'form_display_mode' => 'default',
                        'default_paragraph_type' => 'product_info_nutri',
                        'features' => [
                            'add_above' => 'add_above',
                            'collapse_edit_all' => 'collapse_edit_all',
                            'duplicate' => 'duplicate',
                        ]
                    ],
                ])
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        // Image field paragraph nutritional information.
        $fields['paragraph_nutri_image'] = BaseFieldDefinition::create('image')
            ->setLabel(t('Imagem do Gráfico de Informações Nutricionais'))
            ->setDescription(t('A imagem usada no gráfico de informações nutricionais.'))
            ->setRequired(FALSE)
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE)
            ->setDisplayOptions('form',  [
                'type' => 'image',
                'weight' => 12,
                'label' => 'hidden',
                'settings' => [
                    'image_style' => 'thumbnail',
                ]
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE)
            ->setReadOnly(TRUE);

        // Paragraph for meal nutritional information
        $fields['paragraph_meal'] = BaseFieldDefinition::create('entity_reference_revisions')
            ->setLabel(t('Informações Nutricionais por Refeição'))
            ->setDescription(t('As informações nutricionais do produto por refeição.'))
            ->setRequired(FALSE)
            ->setCardinality(-1)
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'paragraph')
            ->setSetting('handler', 'default:paragraph')
            ->setSetting('handler_settings', [
                'target_bundles' => [
                    'product_info_meal' => 'product_info_meal'
                ],
            ])
            ->setTranslatable(FALSE)
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_paragraphs',
                'weight' => 13,
                'settings' => [
                    'title' => 'Refeição',
                    'title_plural' => 'Refeições',
                    'edit_mode' => 'closed_expand_nested',
                    'closed_mode' => 'summary',
                    'autocollapse' => 'none',
                    'add_mode' => 'modal',
                    'form_display_mode' => 'default',
                    'default_paragraph_type' => 'product_info_meal',
                    'features' => [
                        'add_above' => 'add_above',
                        'collapse_edit_all' => 'collapse_edit_all',
                        'duplicate' => 'duplicate',
                    ]
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Criado'))
            ->setDescription(t('O tempo em que o produto foi criado.'))
            ->setTranslatable(TRUE)
            ->setDisplayConfigurable('view', TRUE);
      
        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Alterado'))
            ->setDescription(t('O tempo em que o produto sofreu a última alteração.'))
            ->setTranslatable(TRUE);

        return $fields;
    }
}