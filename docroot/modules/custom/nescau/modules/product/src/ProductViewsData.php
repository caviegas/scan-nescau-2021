<?php

namespace Drupal\nescau_product;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the product entity.
 */
class ProductViewsData extends EntityViewsData
{
    /**
     * {@inheritdoc}
     */
    public function getViewsData()
    {
        $data = parent::getViewsData();
        return $data;
    }
}