<?php

namespace Drupal\nescau_content_banner\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
//use Drupal\node\Entity\Node;
//use Drupal\trialmachine_campaign\Entity\Campaign;
//use Drupal\Core\Datetime\DrupalDateTime;
//use Drupal\Core\Entity\Query\QueryInterface;
//Use \Drupal\taxonomy\Entity\Term;
//use Drupal\meeg_content\Controller\UserController;


/**
 * Provides a Block for banners.
 *
 * @Block(
 *   id = "banner_block",
 *   admin_label = "Banner",
 *   category = "Nescau",
 * )
 */
class BannerBlock extends BlockBase
{

    /**
     * Entity Node.
     * 
     * @var Drupal\node\Entity\Node
     */
    protected $node;

    //TODO: Por algum motivo isso não funcionou. Tem que verificar.
    // ! ERRO: TypeError: Argument 1 passed to Drupal\nescau_content_banner\Plugin\Block\BannerBlock::__construct() must implement interface Drupal\Core\Entity\EntityStorageInterface, array given, called in C:\wamp64\www\meeg\dev\nescau-2021\web\core\lib\Drupal\Core\Plugin\Factory\ContainerFactory.php on line 25 in Drupal\nescau_content_banner\Plugin\Block\BannerBlock->__construct() (line 36 of modules\custom\nescau\modules\content_banner\src\Plugin\Block\BannerBlock.php).
    /*
    public function __construct(EntityStorageInterface $node)
    {
        $this->node = $node;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity_type.manager')->getStorage('node'),
        );
    }
    */

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        //TODO: Remover depois de resolver o problema do construct
        $this->node = \Drupal::entityTypeManager()->getStorage('node');

        $banner_ids = $this->node->getQuery()
            ->condition('type', 'nescau_content_banner')
            ->condition('status', true)
            ->sort('content_weight', 'ASC')
            ->execute();

        $banners = $this->node->loadMultiple($banner_ids);

        $data = [
            'banners' => $banners,
        ];

        return [
            '#theme' => 'banner_block',
            '#data' => $data,
        ];
    }

    /**
     * @return int
     */
    public function getCacheMaxAge()
    {
        return 0;
    }
}