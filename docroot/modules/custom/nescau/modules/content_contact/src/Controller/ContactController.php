<?php

namespace Drupal\nescau_content_contact\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;

class ContactController extends ControllerBase
{
    /**
     * Node entity storage.
     * 
     * @var Drupal\node\Entity\Node
     */
    protected $node;

    /**
     * ID of the type Contact.
     * 
     * @var string
     */
    protected $id = 'nescau_content_contact';

    /**
     * Implements construct method.
     */
    public function __construct(EntityStorageInterface $node)
    {
        $this->node = $node;
    }

    /**
     * Implements create method.
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity_type.manager')->getStorage('node'),
        );
    }

    /**
     * Page view of Contact page.
     */
    public function view()
    {
        $contact = $this->node->loadByProperties([
            'type' => $this->id,
            'status' => 1,
        ]);

        $addresses = [];
        $chat = [];
        $emails = [];
        $phones = [];
        $sms = [];

        foreach ($contact as $node) {
            $content_addresses = $node->get('paragraph_text_addresses');
            $content_chat = $node->get('paragraph_text_chat');
            $content_emails = $node->get('paragraph_text_emails');
            $content_phones = $node->get('paragraph_text_phones');
            $content_sms = $node->get('paragraph_text_sms');

            if (!empty($content_addresses)) {
                foreach ($content_addresses as $content) {
                    if ($content->entity->get('status')->value == true) {
                        $addresses[] = $content->entity;
                    }
                }
            }

            if (!empty($content_chat)) {
                foreach ($content_chat as $content) {
                    if ($content->entity->get('status')->value == true) {
                        $chat[] = $content->entity;
                    }
                }
            }

            if (!empty($content_emails)) {
                foreach ($content_emails as $content) {
                    if ($content->entity->get('status')->value == true) {
                        $emails[] = $content->entity;
                    }
                }
            }

            if (!empty($content_phones)) {
                foreach ($content_phones as $content) {
                    if ($content->entity->get('status')->value == true) {
                        $phones[] = $content->entity;
                    }
                }
            }

            if (!empty($content_sms)) {
                foreach ($content_sms as $content) {
                    if ($content->entity->get('status')->value == true) {
                        $sms[] = $content->entity;
                    }
                }
            }
        }

        $data = [
            'contact' => $contact,
            'addresses' => $addresses,
            'chat' => $chat,
            'emails' => $emails,
            'phones' => $phones,
            'sms' => $sms,
        ];

        return [
            '#theme' => 'nescau_contact',
            '#data' => $data,
        ];
    }
}