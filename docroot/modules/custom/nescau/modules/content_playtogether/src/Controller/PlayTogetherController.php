<?php

namespace Drupal\nescau_content_playtogether\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;

class PlayTogetherController extends ControllerBase
{
    /**
     * Node entity storage.
     * 
     * @var Drupal\node\Entity\Node
     */
    protected $node;

    /**
     * ID of the type PlayTogether.
     * 
     * @var string
     */
    protected $id = 'nescau_content_playtogether';

    /**
     * Implements construct method.
     */
    public function __construct(EntityStorageInterface $node)
    {
        $this->node = $node;
    }

    /**
     * Implements create method.
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity_type.manager')->getStorage('node'),
        );
    }

    /**
     * Page view of Play Together.
     */
    public function view()
    {
        $playtogether = $this->node->loadByProperties([
            'type' => $this->id,
            'status' => 1,
        ]);

        $paragraphs = [];

        foreach ($playtogether as $node) {
            foreach ($node->get('nescau_paragraph_full') as $paragraph) {
                if ($paragraph->entity->get('status')->value == true) {
                    $paragraphs[] = $paragraph->entity;
                }
            }
        }

        $data = [
            'playtogether' => $playtogether,
            'paragraphs' => $paragraphs,
        ];

        return [
            '#theme' => 'nescau_play_together',
            '#data' => $data,
        ];
    }
}