<?php

namespace Drupal\nescau_recipe\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides a Block for recent recipes.
 *
 * @Block(
 *   id = "recent_recipes",
 *   admin_label = "Receitas recentes",
 *   category = "Nescau",
 * )
 */
class RecentRecipesBlock extends BlockBase
{
    /**
     * Entity Node.
     * 
     * @var Drupal\node\Entity\Node
     */
    protected $node;

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        //TODO: Remover depois de resolver o problema do construct
        $this->node = \Drupal::entityTypeManager()->getStorage('node');

        $recipe_ids = $this->node->getQuery()
            ->condition('type', 'nescau_recipe')
            ->condition('status', true)
            ->sort('created', 'DESC')
            ->range(0, 10)
            ->execute();

        $recipes = $this->node->loadMultiple($recipe_ids);

        $data = [
            'recipes' => $recipes,
        ];

        return [
            '#theme' => 'recent_recipes_block',
            '#data' => $data,
        ];
    }

    /**
     * @return int
     */
    public function getCacheMaxAge()
    {
        return 0;
    }
}