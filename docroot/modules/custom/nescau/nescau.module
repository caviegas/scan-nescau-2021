<?php

use Drupal\views\ViewExecutable;

/**
 * Implements hook_preprocess_HOOK().
 */
function nescau_preprocess_page(&$variables)
{
    $is_front = \Drupal::service('path.matcher')->isFrontPage();

    if ($is_front == true) {
        $classes = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
            'vid' => 'class'
        ]);

        foreach ($classes as $key => $class) {
            $path = (string) str_replace('-', '_', substr($class->toUrl()->toString(), 1));
            $classes[$path] = $class;
            unset($classes[$key]);
        }

        $variables['page']['classes'] = $classes;
    }
}

/**
 * Implements hook_install.
 */
function nescau_install()
{
    $taxonomyVocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');

    $vocabulary = $taxonomyVocabulary->loadByProperties(['vid' => 'class']);

    if (empty($vocabulary)) {
        $vocabulary = $taxonomyVocabulary->create([
            'vid' => 'class',
            'name' => 'Categorias',
            'description' => 'Categorias de produtos',
        ]);
        $vocabulary->save();
    }

    $taxonomyTerm = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    $terms = $taxonomyTerm->loadByProperties([
        'vid' => 'class',
        'name' => [
            'Em Pó',
            'Para Beber',
            'Cereal',
            'Outros Produtos NESCAU®',
        ],
    ]);

    $classes = [
        'Em Pó' => [
            'name' => 'Em Pó',
            'langcode' => 'pt-br',
            'description' => '<p>Produto em pó.</p>',
            'weight' => 1,
            'anchor' => '#em-po',
            'path' => '/em-po',
        ],
        'Para Beber' => [
            'name' => 'Para Beber',
            'langcode' => 'pt-br',
            'description' => '<p>Produto para beber.</p>',
            'weight' => 2,
            'anchor' => '#para-beber',
            'path' => '/para-beber',
        ],
        'Cereal' => [
            'name' => 'Cereal',
            'langcode' => 'pt-br',
            'description' => '<p>Produto cereal.</p>',
            'weight' => 3,
            'anchor' => '#cereal',
            'path' => '/cereal',
        ],
        'Outros Produtos NESCAU®' => [
            'name' => 'Outros Produtos NESCAU®',
            'langcode' => 'pt-br',
            'description' => '<p>Produto diversos.</p>',
            'weight' => 9,
            'anchor' => '#outros-produtos',
            'path' => '/outros-produtos',
        ],
    ];

    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_name = $term->get('name')->value;
            if (array_key_exists($term_name, $classes)) {
                unset($classes[$term_name]);
            }
        }
    }

    foreach ($classes as $class) {
        $term_class = $taxonomyTerm->create([
            'vid' => 'class',
            'langcode' => $class['langcode'],
        ]);
        $term_class->setName($class['name']);
        $term_class->setDescription($class['description']);
        $term_class->setFormat('basic_html');
        $term_class->setWeight($class['weight']);
        $term_class->set('class_anchor', $class['anchor']);
        $term_class->set('path', $class['path']);
        $term_class->save();
    }
}

function nescau_update_9001()
{
    $fieldStorageDefinition = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Conteúdo - Como é feito'))
        ->setDescription(t('O bloco de conteúdo "como é feito".'))
        ->setRequired(FALSE)
        ->setSettings([
            'target_type' => 'node',
            'handler' => 'default',
            'handler_settings' => [
                'target_bundles' => [
                    'nescau_content_howismade' => 'nescau_content_howismade'
                ],
            ],
        ])
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'entity_reference_label',
            'weight' => 2,
        ])
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_autocomplete',
            'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => 60,
                'autocomplete_type' => 'tags',
                'placeholder' => '',
            ],
            'weight' => 2,
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);
  
  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('content_howismade', 'nescau_product', 'nescau_product', $fieldStorageDefinition);
}

function nescau_update_9002()
{
    $fieldStorageDefinition = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Conteúdo - Como preparar'))
        ->setDescription(t('O bloco de conteúdo "como preparar".'))
        ->setRequired(FALSE)
        ->setSettings([
            'target_type' => 'node',
            'handler' => 'default',
            'handler_settings' => [
                'target_bundles' => [
                    'nescau_content_howtoprepare' => 'nescau_content_howtoprepare'
                ],
            ],
        ])
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'entity_reference_label',
            'weight' => 3,
        ])
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_autocomplete',
            'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => 60,
                'autocomplete_type' => 'tags',
                'placeholder' => '',
            ],
            'weight' => 3,
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);
  
  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('content_howtoprepare', 'nescau_product', 'nescau_product', $fieldStorageDefinition);
}

//function nescau_update_9003()
//{
//    $configFactory = \Drupal::configFactory();
//
//    /** field.storage.paragraph.content_text */
//    $config = $configFactory->getEditable('field.storage.paragraph.content_text');
//    $config->set('type', 'text_long');
//    $config->set('module', 'text');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** field.field.paragraph.nescau_text_image.content_text */
//    $config = $configFactory->getEditable('field.field.paragraph.nescau_text_image.content_text');
//    $config->set('field_type', 'text_long');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** field.field.paragraph.nescau_text_image.content_image */
//    $config = $configFactory->getEditable('field.field.paragraph.nescau_text_image.content_image');
//
//    $settings = $config->get('settings');
//    $settings['file_extensions'] = $settings['file_extensions'] . " webp";
//    $config->set('settings', $settings);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** core.entity_form_display.node.nescau_content_howismade.default */
//    $config = $configFactory->getEditable('core.entity_form_display.node.nescau_content_howismade.default');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['config'][] = 'field.field.node.nescau_content_howismade.content_description';
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $content = $config->get('content');
//    $content['content_description'] = [
//        'weight' => 1,
//        'settings' => [
//            'size' => 60,
//            'placeholder' => '',
//        ],
//        'third_party_settings' => [],
//        'type' => 'text_textfield',
//        'region' => 'content',
//    ];
//    $config->set('content', $content);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** core.entity_view_display.node.nescau_content_howismade.default */
//    $config = $configFactory->getEditable('core.entity_view_display.node.nescau_content_howismade.default');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['config'][] = 'field.field.node.nescau_content_howismade.content_description';
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $hidden = $config->get('hidden');
//    $hidden['content_description'] = true;
//    $config->set('hidden', $hidden);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** core.entity_view_display.node.nescau_content_howismade.teaser */
//    $config = $configFactory->getEditable('core.entity_view_display.node.nescau_content_howismade.teaser');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['config'][] = 'field.field.node.nescau_content_howismade.content_description';
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $hidden = $config->get('hidden');
//    $hidden['content_description'] = true;
//    $config->set('hidden', $hidden);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** core.entity_form_display.node.nescau_content_howtoprepare.default */
//    $config = $configFactory->getEditable('core.entity_form_display.node.nescau_content_howtoprepare.default');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['config'][] = 'field.field.node.nescau_content_howtoprepare.content_description';
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $content = $config->get('content');
//    $content['content_description'] = [
//        'weight' => 1,
//        'settings' => [
//            'size' => 60,
//            'placeholder' => '',
//        ],
//        'third_party_settings' => [],
//        'type' => 'text_textfield',
//        'region' => 'content',
//    ];
//    $config->set('content', $content);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** core.entity_view_display.node.nescau_content_howtoprepare.default */
//    $config = $configFactory->getEditable('core.entity_view_display.node.nescau_content_howtoprepare.default');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['config'][] = 'field.field.node.nescau_content_howtoprepare.content_description';
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $hidden = $config->get('hidden');
//    $hidden['content_description'] = true;
//    $config->set('hidden', $hidden);
//
//    $config->save(TRUE);
//    /** --- */
//
//    /** core.entity_view_display.node.nescau_content_howtoprepare.teaser */
//    $config = $configFactory->getEditable('core.entity_view_display.node.nescau_content_howtoprepare.teaser');
//
//    $dependencies = $config->get('dependencies');
//    $dependencies['config'][] = 'field.field.node.nescau_content_howtoprepare.content_description';
//    $dependencies['module'][] = 'text';
//    $config->set('dependencies', $dependencies);
//
//    $hidden = $config->get('hidden');
//    $hidden['content_description'] = true;
//    $config->set('hidden', $hidden);
//
//    $config->save(TRUE);
//    /** --- */
//
//}

function nescau_update_9004()
{
    $fieldStorageDefinition = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference_revisions')
        ->setLabel(t('Gráfico de Informações Nutricionais'))
        ->setDescription(t('O gráfico de informações nutricionais do produto.'))
        ->setRequired(FALSE)
        ->setCardinality(-1)
        ->setRevisionable(TRUE)
        ->setSetting('target_type', 'paragraph')
        ->setSetting('handler', 'default:paragraph')
        ->setSetting('handler_settings', [
            'target_bundles' => [
                'product_info_nutri' => 'product_info_nutri'
            ],
        ])
        ->setTranslatable(FALSE)
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_paragraphs',
            'weight' => 5,
            'settings' => [
                'title' => 'Conteúdo',
                'title_plural' => 'Conteúdos',
                'edit_mode' => 'open',
                'add_mode' => 'dropdown',
                'form_display_mode' => 'default',
                'default_paragraph_type' => '',
            ],
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    \Drupal::entityDefinitionUpdateManager()
        ->installFieldStorageDefinition('paragraph_nutri', 'nescau_product', 'nescau_product', $fieldStorageDefinition);
}

function nescau_update_9005()
{
    $fieldStorageDefinition = \Drupal\Core\Field\BaseFieldDefinition::create('image')
        ->setLabel(t('Imagem do Gráfico de Informações Nutricionais'))
        ->setDescription(t('A imagem usada no gráfico de informações nutricionais.'))
        ->setRequired(FALSE)
        ->setRevisionable(TRUE)
        ->setTranslatable(TRUE)
        ->setDisplayOptions('view', [
            'type' => 'image',
            'weight' => 2,
            'label' => 'hidden',
            'settings' => [
                'image_style' => 'large',
            ],
        ])
        ->setDisplayOptions('form',  [
            'type' => 'image',
            'weight' => 2,
            'label' => 'hidden',
            'settings' => [
                'image_style' => 'thumbnail',
            ]
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE)
        ->setReadOnly(TRUE);
    
    \Drupal::entityDefinitionUpdateManager()
        ->installFieldStorageDefinition('paragraph_nutri_image', 'nescau_product', 'nescau_product', $fieldStorageDefinition);
}

function nescau_update_9006()
{
    $fieldStorageDefinition = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference_revisions')
        ->setLabel(t('Informações Nutricionais por Refeição'))
        ->setDescription(t('As informações nutricionais do produto por refeição.'))
        ->setRequired(FALSE)
        ->setCardinality(-1)
        ->setRevisionable(TRUE)
        ->setSetting('target_type', 'paragraph')
        ->setSetting('handler', 'default:paragraph')
        ->setSetting('handler_settings', [
            'target_bundles' => [
                'product_info_meal' => 'product_info_meal'
            ],
        ])
        ->setTranslatable(FALSE)
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_paragraphs',
            'weight' => 13,
            'settings' => [
                'title' => 'Refeição',
                'title_plural' => 'Refeições',
                'edit_mode' => 'closed_expand_nested',
                'closed_mode' => 'summary',
                'autocollapse' => 'none',
                'add_mode' => 'modal',
                'form_display_mode' => 'default',
                'default_paragraph_type' => 'product_info_meal',
                'features' => [
                    'add_above' => 'add_above',
                    'collapse_edit_all' => 'collapse_edit_all',
                    'duplicate' => 'duplicate',
                ]
            ],
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    \Drupal::entityDefinitionUpdateManager()
        ->installFieldStorageDefinition(
            'paragraph_meal',
            'nescau_product',
            'nescau_product',
            $fieldStorageDefinition
        );
}

function nescau_update_9007()
{
    $title = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('title', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'hidden',
            'type' => 'string',
            'weight' => 0,
        ])
        ->setDisplayOptions('form', [
            'type' => 'string_textfield',
            'weight' => 0,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($title);

    //$path = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('path', 'nescau_product')
    //    ->setDisplayOptions('form', [
    //        'type' => 'path',
    //        'weight' => 1,
    //    ]);
    //\Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($path);

    $tid = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('tid', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'entity_reference_label',
            'weight' => 2,
        ])
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_autocomplete',
            'weight' => 2,
            'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => 60,
                'autocomplete_type' => 'tags',
                'placeholder' => '',
            ],
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($tid);

    $image = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('image', 'nescau_product')
        ->setDisplayOptions('view', [
            'type' => 'image',
            'weight' => 3,
            'label' => 'hidden',
            'settings' => [
                'image_style' => 'large',
                'file_extensions' => 'png gif jpg jpeg webp',
            ],
        ])
        ->setDisplayOptions('form',  [
            'type' => 'image',
            'weight' => 3,
            'label' => 'hidden',
            'settings' => [
                'image_style' => 'thumbnail',
                'file_extensions' => 'png gif jpg jpeg webp',
            ]
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($image);

    $about = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('about', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'hidden',
            'type' => 'text_textarea_with_summary',
            'weight' => 4,
        ])
        ->setDisplayOptions('form', [
            'type' => 'text_textarea_with_summary',
            'settings' => array(
                'rows' => 4,
            ),
            'weight' => 4,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($about);

    $lett_id = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('lett_id', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'hidden',
            'type' => 'string',
            'weight' => 5,
        ])
        ->setDisplayOptions('form', [
            'type' => 'string_textfield',
            'weight' => 5,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($lett_id);

    $components = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('components', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'hidden',
            'type' => 'text_textarea_with_summary',
            'weight' => 6,
        ])
        ->setDisplayOptions('form', [
            'type' => 'text_textarea_with_summary',
            'settings' => array(
                'rows' => 4,
            ),
            'weight' => 6,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($components);

    $preservation = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('preservation', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'hidden',
            'type' => 'text_textarea_with_summary',
            'weight' => 7,
        ])
        ->setDisplayOptions('form', [
            'type' => 'text_textarea_with_summary',
            'settings' => array(
                'rows' => 4,
            ),
            'weight' => 7,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($preservation);

    $nutritional = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('nutritional', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'hidden',
            'type' => 'text_textarea_with_summary',
            'weight' => 8,
        ])
        ->setDisplayOptions('form', [
            'type' => 'text_textarea_with_summary',
            'settings' => array(
                'rows' => 4,
            ),
            'weight' => 8,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($nutritional);

    $content_howismade = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('content_howismade', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'entity_reference_label',
            'weight' => 9,
        ])
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_autocomplete',
            'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => 60,
                'autocomplete_type' => 'tags',
                'placeholder' => '',
            ],
            'weight' => 9,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($content_howismade);

    $content_howtoprepare = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('content_howtoprepare', 'nescau_product')
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'entity_reference_label',
            'weight' => 10,
        ])
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_autocomplete',
            'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => 60,
                'autocomplete_type' => 'tags',
                'placeholder' => '',
            ],
            'weight' => 10,
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($content_howtoprepare);

    $paragraph_nutri = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('paragraph_nutri', 'nescau_product')
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_paragraphs',
            'weight' => 11,
            'settings' => [
                'title' => 'Conteúdo',
                'title_plural' => 'Conteúdos',
                'edit_mode' => 'closed_expand_nested',
                'closed_mode' => 'summary',
                'autocollapse' => 'none',
                'add_mode' => 'modal',
                'form_display_mode' => 'default',
                'default_paragraph_type' => 'product_info_nutri',
                'features' => [
                    'add_above' => 'add_above',
                    'collapse_edit_all' => 'collapse_edit_all',
                    'duplicate' => 'duplicate',
                ]
            ],
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($paragraph_nutri);

    $paragraph_nutri_image = \Drupal::entityDefinitionUpdateManager()->getFieldStorageDefinition('paragraph_nutri_image', 'nescau_product')
        ->setDisplayOptions('form',  [
            'type' => 'image',
            'weight' => 12,
            'label' => 'hidden',
            'settings' => [
                'image_style' => 'thumbnail',
                'file_extensions' => 'png gif jpg jpeg webp',
            ]
        ]);
    \Drupal::entityDefinitionUpdateManager()->updateFieldStorageDefinition($paragraph_nutri_image);
}